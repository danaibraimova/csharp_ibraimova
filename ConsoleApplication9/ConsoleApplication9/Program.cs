﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication9
{
    class Program
    {
        abstract class Figure
        {
            abstract public double Area();
        }
        class Rectangle : Figure
        {
            int w = 0;
            int h = 0;
            public Rectangle(int width, int height)
            {
                w = width;
                h = height;
            }
            public override double Area()
            {
                return w * h;
            }
        }
        class Circle : Figure
        {
            double r = 0;
            public Circle(int radius)
            {
                r = radius;
            }
            public override double Area()
            {
                return Math.PI * r * r;
            }
        }

        static void Main(string[] args)
        {
            Rectangle r = new Rectangle(15, 9);
            Console.WriteLine("Rectangle: {0}", r.Area());
            Circle c = new Circle(9);
            Console.WriteLine("Circle: {0}", c.Area());
            Console.ReadKey();
        }
    }
}
