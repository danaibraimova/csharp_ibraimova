﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace snake
{
    public partial class Form1 : Form
    {
        static int x = 25;
        static int y = 25;
        public Form1()
        {
            InitializeComponent();
        }
        private void button1_Click(object sender, EventArgs e)
        {
            Graphics g = this.CreateGraphics();
            Pen p = new Pen(Color.Orange, 25);
            g.DrawEllipse(p, x, y, 25, 25);
        }
        private void Form1_KeyDown(object sender, KeyEventArgs e)
        {
            Graphics g = this.CreateGraphics();
            g.Clear(Color.White);
            Pen p = new Pen(Color.Orange, 25);
            switch (e.KeyCode)
            {
                case Keys.Right:
                    x += 10;
                    g.DrawEllipse(p, x, y, 25, 25);
                    break;
                case Keys.Left:
                    x -= 10;
                    g.DrawEllipse(p, x, y, 25, 25);
                    break;
                case Keys.Down:
                    y -= 10;
                    g.DrawEllipse(p, x, y, 25, 25);
                    break;
                case Keys.Up:
                    y += 10;
                    g.DrawEllipse(p, x, y, 25, 25);
                    break;
                default:
                    break;

            }
            Invalidate();//перерисовка
            Update();
        }
    }
}