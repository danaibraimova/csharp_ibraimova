﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace levenstein_mf
{
    public class Levenshtein
    {
        public static int Distance(string x, string y)
        {
            int n = x.Length;
            int m = y.Length;
            int[,] d = new int[n + 1, m + 1];//пустой двумерный массив
            if (n == 0)
            {
                return m;
            }
            if (m == 0)
            {
                return n;
            }
            for (int i = 0; i <= n; i++)
            {
                d[i, 0] = i;
            }
            for (int j = 0; j <= m; j++)
            {
                d[0, j] = j;
            }
            for (int i = 1; i <= n; i++)
            {
                for (int j = 1; j <= m; j++)
                {
                    int num = (y[j - 1] == x[i - 1] ? 0 : 1);//если последние элементы совпадают, количество перестановок равно 0, иначе - 1
                    d[i, j] = Math.Min(Math.Min(d[i - 1, j] + 1, d[i, j - 1] + 1), d[i - 1, j - 1] + num);
                }
            }
            return d[n, m];
        }

    }
    class Program
    {
        static void Main(string[] args)
        {
            StreamReader sr = new StreamReader(@"E:\words.txt");
            string[] dict = new string[10000];
            string s = @"llama";
            string[] word = s.Split();
            int size = word.Length;
            while (sr.Peek() != -1)
            {
                for (int i = 0; i < size; i++)
                {
                    dict[i] = sr.ReadLine();
                }
            }
            for (int i = 0; i < word.Length; i++)
            {
                int min_dist = 0;
                for (int j = 0; j < size; j++)
                {
                    if (Levenshtein.Distance(word[i], dict[j]) > min_dist) //|| min_dist == -1)
                    {
                        min_dist = Levenshtein.Distance(word[i], dict[j]);
                        Console.WriteLine(min_dist);
                        Console.WriteLine("{0}, {1}", word[i], dict[j]);
                    }
                    if (min_dist == 0)
                    {
                        Console.WriteLine("Correct");
                    }
                }

                //else
                // {
                //for (int j = 0; j < sz; j++)
                // {
                //  if (Distance(word[i], dict[j]) == min_dist)
                //{
                //  Console.WriteLine(dict[j]);
                //}
                //}
                //}
                Console.ReadKey();
            }
        }
    }
}
        
