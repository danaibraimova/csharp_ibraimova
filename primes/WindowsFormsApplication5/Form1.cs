﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplication5
{

    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        int a = 100;
        float x = 0;
        float y = 0; 
        int i = 1;
        private void timer1_Tick(object sender, EventArgs e)
        {
            Graphics g = this.CreateGraphics();
            int v = 0;
            for (int j = 1; j <= i && i<=a; j++)
            {
                if (i % j == 0)
                {
                    v += j;
                }
                if (v - 1 == i)
                {
                    g.DrawString(i.ToString(), new Font("AR HERMANN", 20), new SolidBrush(Color.Black), new PointF(x, y));
                }
            }
            i++;
            x += 15;
        }
    }
}
