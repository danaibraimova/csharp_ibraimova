﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab1Part2Ex1
{
    class Program
    {
        static void Main(string[] args)
        {
            string s = "Microsoft .NET Framework 2.0 Application Development Foundation";
            string[] sa = s.Split(' ');//separates the string into an array of words
            Array.Sort(sa);
            s = string.Join(" ", sa);//converts the array of words back into a single string
            Console.WriteLine(s);
        }
    }
}
