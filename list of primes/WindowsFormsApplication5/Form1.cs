﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplication5
{

    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        int a = 100;
        int x = 0;
        int y = 0;
        int j = 2;
        static bool isPrime(int a)
        {
            if (a < 2)
            {
                return false;
            }
            for (int i = 2; i <= Math.Sqrt(a); i++)
            {
                if (a % i == 0)
                {
                    return false;
                }
            }
            return true;
        }
        private void timer1_Tick(object sender, EventArgs e)
        {
            Graphics g = this.CreateGraphics();
                if(isPrime(j))
                {
                    g.DrawString(j.ToString(), new Font("Arial", 20), new SolidBrush(Color.Black), new Point(x, y));
                }
                j++;
                x += 15;
            if(j==a)
            {
                this.Close();
            }

            
            
        }
    }
}
