﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace calculator
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string s = textBox1.Text;
            int a = int.Parse(s);
            string s2 = textBox2.Text;
            int b = int.Parse(s2);
            label1.Text = (a + b).ToString();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            string s = textBox1.Text;
            int a = int.Parse(s);
            string s2 = textBox2.Text;
            int b = int.Parse(s2);
            label1.Text = (a - b).ToString();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            string s = textBox1.Text;
            int a = int.Parse(s);
            string s2 = textBox2.Text;
            int b = int.Parse(s2);
            label1.Text = (a * b).ToString();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            string s = textBox1.Text;
            double a = int.Parse(s);
            string s2 = textBox2.Text;
            double b = int.Parse(s2);
            label1.Text = (a / b).ToString();
        }

      
    }
}
