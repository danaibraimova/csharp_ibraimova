﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace Lab2Ex2
{

    class Program
    {
        static void watcher_changed(object sender, FileSystemEventArgs e)
        {
            Console.WriteLine("Changed: {0}", e.FullPath);
        }
        static void Main(string[] args)
        {
            
            FileSystemWatcher watcher = new FileSystemWatcher(@"c:");
            watcher.Filter = "*.ini";
            watcher.IncludeSubdirectories = true;
            watcher.NotifyFilter = NotifyFilters.Attributes | NotifyFilters.Size;
            watcher.Changed += new FileSystemEventHandler(watcher_changed);
            watcher.EnableRaisingEvents = true;
        }
    }
}
