﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Collections;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Xml.Serialization;
namespace mid
{
    class Program
    {
        public class Ship
        {
            public int x, y, dir, length;
            public Ship(int _x, int _y, int _dir, int _length)
            {
                x = _x;
                y = _y;
                dir = _dir;
                length = _length;
            }
            public override string ToString()
            {
                return (x + y + dir + length).ToString();
            }
        }
        [Serializable()]
        
        public class SeaBattle : ISerializable
        {
            List<Ship> a = new List<Ship>();
            //int[,] a = new int[10, 10];
            public SeaBattle(SeaBattle _a)
            {
               StreamReader fs = new StreamReader("sb.txt");
               string s = fs.ReadToEnd();
               string[] ships = s.Split();
               foreach(Ship ship in a)
               {
                   for(int i=0;i<4;i++)
                   {
                       ship.x=Convert.ToInt32(ships[i]);
                       ship.y=Convert.ToInt32(ships[i+1]);
                       ship.dir=Convert.ToInt32(ships[i+1]);
                       ship.length=Convert.ToInt32(ships[i+1]);
                   }
               }
                public override string ToString()
                {
                    char sign='*';
                    a=null;
                    foreach(Ship ship in a)
                    {
                        Console.Write(sign);
                    }
                    return a.ToString();
                }
            public int check()
            {
                int[,] a = new int[10, 10];
                
                for(int i=0;i<4;i++)
                {
                    for(int j=0;j<4;j++)
                    {  
                        if((a[i,j]==1)||(a[j,i]==1)||(a[i+1,j]==1)||(a[i,j+1]==1)||(a[i-1,j]==1)||a[i,j-1]==1)
                        {
                            return 1;
                        }
                        if((a[i,j]==2)||(a[j,i]==2)||(a[i+1,j]==2)||(a[i,j+1]==2)||(a[i-1,j]==2)||a[i,j-1]==2)
                        {
                            return 1;
                        }
                        if((a[i,j]==3)||(a[j,i]==3)||(a[i+1,j]==3)||(a[i,j+1]==3)||(a[i-1,j]==3)||a[i,j-1]==3)
                        {
                            return 1;
                        }
                        if((a[i,j]==4)||(a[j,i]==4)||(a[i+1,j]==4)||(a[i,j+1]==4)||(a[i-1,j]==4)||a[i,j-1]==4)
                        {
                            return 2;
                        }
                        if((a[i,j]==0)||(a[j,i]==0)||(a[i+1,j]==0)||(a[i,j+1]==0)||(a[i-1,j]==0)||a[i,j-1]==0)
                        {
                            return 0;
                        }
                    }
                }
                return check();
            }
    
    
        }

    
    static void Main(string[] args)
    {
        SeaBattle k=new SeaBattle();
        FileStream fs=new FileStream("Serialization.Data",FileMode.Create);
        BinaryFormatter bf=new BinaryFormatter();
        bf.Serialize(fs,k);
    }
    }
}
