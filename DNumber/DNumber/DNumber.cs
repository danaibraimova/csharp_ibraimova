﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DNumber
{
    struct DNumber
    {
        public int a, b;
        public DNumber(int x,int y)
        {
            a = x;
            b = y;
        }
        public override string ToString()
        {
            return a.ToString() + "/" + b.ToString();
        }
        public void normize()
        {
            int x = a;
            int y = b;
            while(x>0 && y>0)
            {
                if(x>y)
                {
                    x = x % y;
                }
                else
                {
                    y = y % x;
                }
                x = x + y;
                a = a / x;
                b = b / x;
            }
        }
        public static DNumber operator *(DNumber arg1,DNumber arg2)
        {
            arg1.a *= arg2.a;
            arg1.b *= arg2.b;
            arg1.normize();
            return arg1;
        }
        public static DNumber operator +(DNumber arg1,DNumber arg2)
        {
            arg1.a = arg1.a * arg2.b + arg2.a * arg1.b;
            arg1.b = arg1.b * arg2.b;
            arg1.normize();
            return arg1;
        }
        public static DNumber operator -(DNumber arg1,DNumber arg2)
        {
            arg1.a = arg1.a * arg2.b - arg2.a * arg1.b;
            arg1.b = arg1.b * arg2.b;
            return arg1;
        }
        public static DNumber operator /(DNumber arg1,DNumber arg2)
        {
            arg1.a = arg1.a * arg2.b;
            arg1.b = arg1.b * arg2.a;
            return arg1;
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            DNumber x = new DNumber(1, 2);
            DNumber y = new DNumber(2, 3);
            DNumber u = x * y;
            DNumber s = x + y;
            DNumber v = x - y;
            DNumber d = x / y;
            Console.WriteLine(u);
            Console.WriteLine(s);
            Console.WriteLine(v);
            Console.WriteLine(d);
            Console.ReadKey();
        }
    }
}
