﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplication4
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        int x1 = 0;
        int x2 = 0;
        private void button1_Click(object sender, EventArgs e)
        {
            Graphics g;//поверхность для рисования  
            g = this.CreateGraphics();
            g.Clear(Color.White);
            Pen p = new Pen(Color.DarkCyan, 15);  
            g.DrawLine(p, x1, 0, x2, 90); 
            x1 += 10;
            x2 += 10;
        }
        private void button2_Click(object sender, EventArgs e)
        {
            Graphics g;//поверхность для рисования  
            g = this.CreateGraphics();
            g.Clear(Color.White);
            Pen p = new Pen(Color.DarkCyan, 15);
            g.DrawLine(p, x1, 0, x2, 90);
            x1 -= 10;
            x2 -= 10;
        }
    }
}
