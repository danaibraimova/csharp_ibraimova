﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Xml.Serialization;

namespace miiiiiiiiiiiiiidddddddddddddddddddddddd
{
    class Program
    {
        class Point
        {
            public int x, y;
            public Point(int _x, int _y)
            {
                x = _x;
                y = _y;
            }
            public override string ToString()
            {
                return x + ", " + y;
            }
            public static Point operator +(Point arg1, Point arg2)
            {
                arg1.x += arg2.x;
                arg1.y += arg2.y;
                return arg1;
            }
        }
        class Line
        {
            public Point a, b;
            public Line(Point _a, Point _b)//для передачи в новые экземпляры
            {
                a = _a;
                b = _b;
            }
            public double lineLength()
            {
                double l = Math.Sqrt((a.x - b.x) ^ 2 + (a.y - b.y) ^ 2);
                return l;
            }
            public override string ToString()
            {
                return lineLength().ToString();
            }
            public static Line operator +(Line arg1, Line arg2)
            {
                arg1+=arg2;
                return arg1;
            }
            
        } 
        [Serializable]
        public class Polygon
        {
           
           //ArrayList points = new ArrayList();
            List<Point> points = new List<Point>();
            public Polygon()
            {
                StreamReader fs = new StreamReader("Polygon.txt",);
                string s = fs.ReadToEnd();
                string[] pps=s.Split(' ');
                int pp = Convert.ToInt32(pps);
                points=pp.ToList<Point>;
            }
            
           public override string ToString()
           {
               foreach(Point p in points)
               {
                   return p.ToString();
               }        
           }
            public Line Perimeter(int l)
            {
                Line perimeter=null;
                for(int i=0;i<points.Count;i++)
                { 
                    Line temp=new Line(points[i],points[i+1]);
                    perimeter+=temp;
                }
                return perimeter;
            }    
        }
    
        static void Main(string[] args)
        {
            Point p1 = new Point(5, 6);
            Point p2 = new Point(6, 7);
            Console.WriteLine("The sum: ");
            Console.WriteLine(p1 + p2);
            Console.WriteLine("The length: ");
            Line len = new Line(p1, p2);
            Console.WriteLine(len);
            FileStream fs = new FileStream("Serialized.txt", FileMode.Create);
            BinaryFormatter bf = new BinaryFormatter();
            bf.Serialize(fs, bf);
            fs.Close();

            Console.ReadKey();
        }
    }
}
