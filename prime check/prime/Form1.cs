﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace prime
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        static bool isPrime(int a)
        {
            if (a < 2)
            {
                return false;
            }
            for (int i = 2; i <= Math.Sqrt(a); i++)
            {
                if (a % i == 0)
                {
                    return false;
                }
            }
            return true;
        }
        
        private void button1_Click(object sender, EventArgs e)
        {
            string s = textBox1.Text;
            int a = int.Parse(s);
            if(isPrime(a))
            {
                label1.Text = "prime";
            }
            else
            {
                label1.Text = "not prime";
            }
        }
    }
}
