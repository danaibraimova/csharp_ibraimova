﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace labels
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        static int i;

        private void button1_Click(object sender, EventArgs e)
        {
            this.Controls.Add(new Label()
            {
                Name="label"+i.ToString(),
                Location = new Point(12, 9 + i*5),
                Size = new System.Drawing.Size(35, 13)
            });
            i++;
        }
    }
}
