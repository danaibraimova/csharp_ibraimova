﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;


namespace Lab3_1Ex1
{
    class Program
    {
        static bool IsPhone(string s)
        {
            return Regex.IsMatch(s, @"^\(?\d{3}\)?[\s\-]?\d{3}\-?\d{4}$");
        }
        static bool IsZip(string s)
        {
            return Regex.IsMatch(s, @"^\d{5}(\-\d{4})?$");
        }
        static void Main(string[] args)
        {
            Console.WriteLine("Enter a phone number: ");
            string ph = Console.ReadLine();
            if(IsPhone(ph))
            {
                Console.WriteLine(ph + " is a phone number");
            }
            else
            {
                Console.WriteLine("unknown");
            }
            Console.WriteLine("Enter a zip code: ");          
            string z = Console.ReadLine();
            if(IsZip(z))
            {
                Console.WriteLine(z + " is a zip code");
            }
            else
            {
                Console.WriteLine("unknown");
            }
                Console.ReadKey();
        }
    }
}
