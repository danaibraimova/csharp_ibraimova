﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigNumber
{
    struct BigNumber
    {
        public int[] a;
        public int size;
        public BigNumber(string s)
        {
            a = new int[1000];
            size = s.Length;
            for (int i = s.Length - 1, j = 0; i >= 0; i--, j++)
            {
                a[j] = s[i] - '0';
            }
        }
        public override string ToString()
        {
            string s = "";
            for (int i = s.Length; i >= 0; i--)
            {
                s += a[i].ToString();
            }
            return s;
        }
        public static BigNumber operator +(BigNumber arg1, BigNumber arg2)
        {
            int temp = 0;
            BigNumber x = new BigNumber("0");
            int len = arg1.size > arg2.size ? arg1.size : arg2.size;
            for (int i = 0; i < len; i++)
            {
                x.a[i] = arg1.a[i] + arg2.a[i] + temp;
                x.a[i] %= 10;
                temp = (arg1.a[i] + arg2.a[i]) / 10;
            }
                if(temp>0)
                {
                   x.size = arg1.size + 1;
                   x.a[len - 1] = temp;
               }
                for(int j=len-1;j>0;j--)
                {
                    Console.WriteLine(x.a[j]);
                }
            
           return x;
        }


        class Program
        {
            static void Main(string[] args)
            {
                BigNumber a = new BigNumber("12345");
                BigNumber b = new BigNumber("6789");
                Console.WriteLine(a+b);
                Console.ReadKey();
            }
        }
    }
}

