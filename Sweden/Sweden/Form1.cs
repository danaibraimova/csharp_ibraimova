﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Sweden
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Graphics g;
            g = this.CreateGraphics();
            Pen p = new Pen(Color.Gold, 50);
            Brush blue = new SolidBrush(Color.DodgerBlue);
            Brush gold = new SolidBrush(Color.Gold);
            g.FillRectangle(blue, 10, 10, 450, 250);
            g.DrawLine(p, 10, 127, 460, 127);
            g.DrawLine(p, 110, 10, 110, 260);


        }
    }
}
