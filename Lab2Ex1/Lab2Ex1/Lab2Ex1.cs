﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace Lab2Ex1
{
    class Program
    {
        static void ShowDirectory(DirectoryInfo dir)
        {
            foreach(FileInfo file in dir.GetFiles())
            {
                Console.WriteLine("File: {0}", file.FullName);
            }
            foreach(DirectoryInfo subDir in dir.GetDirectories())
            {
                ShowDirectory(subDir);
            }
        }  
        static void Main(string[] args)
        {
            DirectoryInfo ourDir = new DirectoryInfo(@"c:");
            ShowDirectory(ourDir);
        }
     }
}
