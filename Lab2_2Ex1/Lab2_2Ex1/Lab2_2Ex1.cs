﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace Lab2_2Ex1
{
    class Program
    {
        static void Main(string[] args)
        {
            StreamWriter writer= File.CreateText(@"c:\newfile.txt");
            writer.WriteLine("Hello, darkness, my old friend");
            writer.WriteLine("I've come to talk with you again");
            writer.Close();
        }
    }
}
