﻿using System;

using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication7
{
    class Program
    {
        public enum Days {Monday, Tuesday, Wednesday, Thursday, Friday, Saturday, Sunday};
        struct Schedule
        {
            Days[] busydays;
            public Schedule(Days[] _busydays)
            {
                busydays = _busydays;
                //_busydays = Enum.GetNames(typeof(Days));
            }
            public override string ToString()
            {
                string s="";
                foreach(Days d in busydays)
                {
                    s=s+", "+d.ToString();
                }
                return s;
            }
        }
            struct Person
            {
                public string fName;
                public string lName;
                public int age;
                public Schedule s;
                public Person(string _fName, string _lName,int _age,Schedule _s)
                {
                    fName=_fName;
                    lName=_lName;
                    age=_age;
                    s=_s;
                }
                public override string ToString()
                {
                    return fName+ " "+lName+", "+age+", "+s;
                }
            }
        static void Main(string[] args)
        {
            Days[] d = new Days[3];
            d[0] = Days.Monday;
            d[1] = Days.Sunday;
            d[2] = Days.Wednesday;
            Schedule sch = new Schedule(d);
            Person p=new Person("Tony", "Allen", 32, sch);
            Console.WriteLine(p);
            Console.ReadKey();

        
        
        }
    }
}
